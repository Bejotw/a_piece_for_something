/*------------------------------------------------------------------
 * bss_global.h
 * Header file for BSS module
 *
 * BeJo, Jun. 2012
 *
 * All rights reserved.
 *------------------------------------------------------------------
 */

#ifndef __BSS_GLOBAL_H__
#define __BSS_GLOBAL_H__

/* Device configuration */
#define INT_NUM			10
#define INT_LEN			8

#endif /* __BSS_GLOBAL_H__ */
