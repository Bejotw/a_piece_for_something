#include <linux/module.h>  
#include <linux/kernel.h> 
#include <linux/init.h> 
#include <net/sock.h>
#include <net/netlink.h>
#include <linux/skbuff.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/stat.h>
#include <linux/fcntl.h>
#include <linux/unistd.h>
#include <linux/delay.h>
#include "kudp.h"
#define SKBBUFF		1500
#define MAX_PAYLOAD 1024 

#ifndef uint32
typedef unsigned int    uint32;
#endif
//static struct pgent_send_info data;
static struct sock *netlink_sock;	
extern void lpgen_info(struct sk_buff *skb);


static void udp_reply(int pid,int seq,void *payload)
{
	printk(KERN_INFO "Entering: %s\n", __FUNCTION__);
	struct sk_buff	*skb;
	struct nlmsghdr	*nlh;
	//int size=strlen((char)payload)+1;
	int		len = NLMSG_SPACE(MAX_PAYLOAD);
	void		*data;
	int ret;
	skb = alloc_skb(len, GFP_ATOMIC);
	if (!skb)
		return;
	nlh= NLMSG_PUT(skb, pid, seq, 0, MAX_PAYLOAD);
	nlh->nlmsg_flags = 0;
	strcpy(NLMSG_DATA(nlh), "Hello World from kernel");
	data=NLMSG_DATA(nlh);
	printk(KERN_INFO "data is :%s\n",(char *)data);
	NETLINK_CB(skb).pid = 0;         /* from kernel */
	NETLINK_CB(skb).dst_group = 0;  /* unicast */
	ret=netlink_unicast(netlink_sock, skb, pid, MSG_DONTWAIT);
	if (ret <0)
	{
		printk("send failed\n");
		return;
	}
	return;
	
nlmsg_failure:			/* Used by NLMSG_PUT */
	if (skb)
		kfree_skb(skb);
}



/* Receive messages from netlink socket. */
static void udp_receive(struct sk_buff  *skb)
{
	
	
  	u_int	uid, pid, seq, sid;
	printk(KERN_INFO "Entering: %s\r\n", __FUNCTION__);
	struct nlmsghdr *nlh = (struct nlmsghdr *)skb->data;
	pid  = NETLINK_CREDS(skb)->pid;
	uid  = NETLINK_CREDS(skb)->uid;
	sid  = NETLINK_CB(skb).sid;
	seq  = nlh->nlmsg_seq;
	lpgen_info(skb);//triger the kernel to send the packet
	printk(KERN_INFO "send the traffic\n");
	return ;
}

static int __init kudp_init(void)
{
	printk(KERN_INFO "Entering: %s\n", __FUNCTION__);
	netlink_sock = netlink_kernel_create(&init_net, NETLINK_USERSOCK, 0,udp_receive, NULL, THIS_MODULE);
	return 0;
}

static void __exit kudp_exit(void)
{	
	printk(KERN_INFO "exiting hello module\n");
	sock_release(netlink_sock->sk_socket);
	printk("netlink driver remove successfully\n");
}
module_init(kudp_init);
module_exit(kudp_exit);

