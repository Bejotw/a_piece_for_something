/*------------------------------------------------------------------
 * lpmd.c
 * This is a loadable module for the Linux kernel for RGMII loopback 
 * with different iuDMA.
 *
 * Version: 1.0
 *
 * BeJo,  Jun. 2012
 *
 * All rights reserved.
 *------------------------------------------------------------------
 */

#include <linux/init.h>
#include <linux/socket.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/timer.h>
#include <linux/capability.h>
#include <linux/skbuff.h>
#include <linux/netlink.h>
#include <net/sock.h>
#include <net/psnap.h>
#include <linux/string.h>
#include <linux/netdevice.h>
#include <linux/inet.h>
#include <linux/ip.h>
#include <linux/if_ether.h>
#include <net/ip.h>
#include <linux/udp.h>
#include <linux/icmp.h>

#include "lpmd.h"
#include "../include/bss_global.h"

#ifndef	uint8
typedef unsigned char   uint8;
#endif
#ifndef uint16
typedef unsigned short  uint16;
#endif
#ifndef uint32
typedef unsigned int    uint32;
#endif
#ifndef uint64
typedef unsigned long long uint64;
#endif

#define SKBBUFF		1500
#define	PACKET_SIZE	1000
#define PAYLOAD_SIZE	100
#define SMAC	0x02, 0x10, 0x18, 0x01, 0x00, 0x01
#define DMAC	0xff, 0xff, 0xff, 0xff, 0xff, 0xff
#define	SIP	0xc0, 0xa8, 0x01, 0x01
#define DIP	0xc0, 0xa8, 0x01, 0x0a

#define MV_DSA_TAG_SIZE 4
#define TX_BUF_SIZE  1536
#if 1
#define GTK_SWDBG(msg, arg...) printk("%s(%d): " msg, __FUNCTION__, __LINE__, ##arg)
#else
#define GTK_SWDBG(msg, arg...)
#endif

static struct proc_dir_entry *proc_lpmd;

/* For Timer */
typedef struct {
	int index;
	char interface[INT_LEN];
    int freq;
    int count;
	int pak_len;
    char *data;
} TIMER_PRIV;
struct timer_list lp_timer[INT_NUM];
TIMER_PRIV timer_data[INT_NUM];

struct traffic_control traffcon;
struct eth_header_info ethhr;
struct ip_header_info iphr;
struct net_header_info nethr;
int lpmd_isEnable = 0;
int send_count = 1;


unsigned char d_mac[]={DMAC};
unsigned char s_mac[]={SMAC};
unsigned char skb_send[150];
//unsigned char packet_data[] = { DMAC, SMAC, 0x08, 0x00, 0x45, 0x00, 0x00, 0x95, 0x3c, 0xf0, 0x00, 0x00, 0x80, 0x11, 0xa5, 0xe2, SIP, DIP, 0x44, 0x5c, 0x44, 0x5c, 0x00, 0x81, 0x8e, 0x55};
unsigned char packet_data[] = { 0x45, 0x00, 0x00, 0x95, 0x3c, 0xf0, 0x00, 0x00, 0x80, 0x11, 0xa5, 0xe2, SIP, DIP, 0x44, 0x5c, 0x44, 0x5c, 0x00, 0x81, 0x8e, 0x55};

void add_marvel_tag(struct sk_buff *skb)
{
	
    skb_push(skb, MV_DSA_TAG_SIZE);
    memmove((void *)skb->data, (void *)(skb->data+MV_DSA_TAG_SIZE), 2*ETH_ALEN);
    skb->data[2*ETH_ALEN] = 0x81;
    skb->data[2*ETH_ALEN+1] = 0x00;
    skb->data[2*ETH_ALEN+2] = 0x00;
    skb->data[2*ETH_ALEN+3] = 0x01;
	
}
unsigned short in_cksum(unsigned short *addr, int len)
{
	register int sum = 0;
	u_short answer = 0;
	register u_short *w = addr;
	register int nleft = len;
	/*
	* Our algorithm is simple, using a 32 bit accumulator (sum), we add
	* sequential 16 bit words to it, and at the end, fold back all the
	* carry bits from the top 16 bits into the lower 16 bits.
	*/
	while (nleft > 1)
	{
		sum += *w++;
		nleft -= 2;
	}
	/* mop up an odd byte, if necessary */
	if (nleft == 1)
	{
		*(u_char *) (&answer) = *(u_char *) w;
		sum += answer;
	}
	/* add back carry outs from top 16 bits to low 16 bits */
		sum = (sum >> 16) + (sum & 0xffff); /* add hi 16 to low 16 */
		sum += (sum >> 16); /* add carry */
		answer = ~sum; /* truncate to 16 bits */
		return (answer);
	}

uint16 udp_checksum(const void *buff, size_t len, uint32 src_addr, uint32 dest_addr)
{
	const uint16 *buf = buff;
	uint16 *ip_src, *ip_dst;
	uint32 sum;
	size_t length = len;

	ip_src = (void *)&src_addr;
	ip_dst = (void *)&dest_addr;

	sum = 0;
	while (len > 1)
	{
		sum += *buf++;
		if (sum & 0x80000000)
			sum = (sum & 0xFFFF) + (sum >> 16);
		len -= 2;
	}

	if ( len & 1 )
		sum += *((uint8 *)buf);

	sum += *(ip_src++);
	sum += *ip_src;

	sum += *(ip_dst++);
	sum += *ip_dst;

	sum += htons(iphr.ip_ptotocol);
	sum += htons(length);

	while (sum >> 16)
		sum = (sum & 0xFFFF) + (sum >> 16);

	return ((uint16)(~sum));

}

int eth_header(struct sk_buff *skb, struct net_device *dev, uint16 type, 
		const void *daddr, const void *saddr, unsigned len)
{
        struct ethhdr *eth = (struct ethhdr *)skb_push(skb, ETH_HLEN);
	
        if (type != ETH_P_802_3)
                eth->h_proto = htons(type);
        else
                eth->h_proto = htons(len);

        if (!saddr)
                saddr = dev->dev_addr;
        memcpy(eth->h_source, saddr, ETH_ALEN);

        if (daddr) {
                memcpy(eth->h_dest, daddr, ETH_ALEN);
                return ETH_HLEN;
        }

        if (dev->flags & (IFF_LOOPBACK | IFF_NOARP)) {
                memset(eth->h_dest, 0, ETH_ALEN);
                return ETH_HLEN;
        }

        return -ETH_HLEN;
}


int ip_header(struct sk_buff *skb, uint32 *daddr, uint32 *saddr)
{
	struct iphdr *iph;

	iph = ip_hdr(skb);
	/* Move Data point */
	skb_put(skb, sizeof(struct iphdr));
	iph->ihl = 5;
	iph->version = 4;	
	iph->tos = 0;
	iph->protocol = iphr.ip_ptotocol;    /* UDP or icmp */
	iph->ttl = iphr.ip_ttl;	
	iph->saddr = iphr.ip_src;
	iph->daddr = iphr.ip_dst;
	iph->frag_off = 0;
	if (iph->protocol==IPPROTO_ICMP) {
		iph->tot_len = htons(sizeof(struct iphdr) + sizeof(struct icmphdr) + PAYLOAD_SIZE);		
		}else{
			iph->tot_len = htons(sizeof(struct iphdr) + sizeof(struct udphdr) + PAYLOAD_SIZE);	
		}
	iph->check = 0;
	iph->check = ip_fast_csum((void *)iph, iph->ihl);
	skb->protocol = htons(ethhr.eth_type);
	skb->pkt_type = PACKET_HOST;
 
	return 1;
}

int net_header(struct sk_buff *skb)
{
	struct iphdr *iph;
	struct udphdr *udph;

	iph = ip_hdr(skb);
	udph = udp_hdr(skb);

	skb_put(skb, sizeof(struct udphdr));
	udph->source = htons(nethr.un.udp.udp_source);
	udph->dest = htons(nethr.un.udp.udp_dest);
	udph->len = htons(sizeof(struct udphdr)+PAYLOAD_SIZE);
	udph->check = 0;

	memset(skb_put(skb, PAYLOAD_SIZE), 0x0, PAYLOAD_SIZE);

	/* Checksum */
	udph->check = 0;
	udph->check = udp_checksum(udph, sizeof(struct udphdr)+PAYLOAD_SIZE, iph->saddr, iph->daddr);

	return 1;
}

int icmp_net_header(struct sk_buff *skb)
{
	struct iphdr *iph;
	struct icmphdr *icmph;

	iph = ip_hdr(skb);
	icmph = icmp_hdr(skb);
	printk(KERN_INFO"check7\r\n");
	skb_put(skb, sizeof(struct icmphdr));
	icmph->type = nethr.un.icmp.icmp_type;
	icmph->code = 0;
	icmph->checksum = 0;
	icmph->un.echo.id = htons(0x1234);
	icmph->un.echo.sequence = htons(1);
	icmph->checksum = in_cksum((unsigned short *)icmph, sizeof(struct icmphdr));
	
	/* Add data meaase to SKB */
	
	skb_put(skb,traffcon.frame_size-(sizeof(struct ethhdr)+sizeof(struct iphdr)+sizeof(struct icmphdr)));
	memcpy(skb->data+(sizeof(struct ethhdr)+sizeof(struct iphdr)+sizeof(struct icmphdr)),nethr.data,traffcon.frame_size-(sizeof(struct ethhdr)+sizeof(struct iphdr)+sizeof(struct icmphdr)));
	add_marvel_tag(skb);

	printk(KERN_INFO"check11\r\n");

	return 0;	
}

int packet_make(struct sk_buff *skb) 
{

        char dev_name[8];
	
	skb_reserve(skb, 2);

        /* getting & checking net device whether exists  */
        memset (dev_name, 0, sizeof(dev_name));
        sprintf(dev_name, "eth1");
        skb->dev = dev_get_by_name(&init_net, dev_name);

        if (!skb->dev) {
                        printk ("%s: __dev_get_by_name(%s) fail\n", __FUNCTION__, dev_name);
                        goto ERR;
        }


	/*  Reserve for ethernet and IP header  */
		eth_header(skb, skb->dev, ETH_P_IP, d_mac, NULL, ETH_P_IP);
	skb->network_header = skb->tail; /* For ip_hdr() */
	skb->transport_header = skb->network_header + sizeof(struct iphdr); /* For udp_hdr */
	
	
	ip_header(skb, NULL, NULL);
	net_header(skb);
	

	return 1;
ERR:
	return -1;

}

int packet_make_frame(struct sk_buff *skb, char *dev_name) 
{
	skb_reserve(skb, 2);
	printk(KERN_INFO"check the skb->len %d\r\n",skb->len);
    	/* getting & checking net device whether exists  */
    	skb->dev = dev_get_by_name(&init_net, dev_name);

    	if (!skb->dev) {
		printk ("%s: __dev_get_by_name(%s) fail\n", __FUNCTION__, dev_name);
        goto ERR;
    	}
	/*  Reserve for ethernet and IP header  */
	eth_header(skb, skb->dev, ethhr.eth_type, ethhr.dmac, NULL, ethhr.eth_type);
	skb->network_header = skb->tail; /* For ip_hdr() */
	skb->transport_header = skb->network_header + sizeof(struct iphdr); /* For udp_hdr */
	
	ip_header(skb, NULL, NULL);
	printk(KERN_INFO"check4\r\n");
	if (iphr.ip_ptotocol == IPPROTO_ICMP){

		icmp_net_header(skb);
	} else {

		printk(KERN_INFO"check5\r\n");
		net_header(skb);
		}

	return 1;
ERR:
	return -1;

}



int packet_make_raw(struct sk_buff *skb, char *dev_name) 
{
	skb_reserve(skb, 2);
	printk(KERN_INFO"check the skb->len %d\r\n",skb->len);
    	/* getting & checking net device whether exists  */
    	skb->dev = dev_get_by_name(&init_net, dev_name);

    	if (!skb->dev) {
		printk ("%s: __dev_get_by_name(%s) fail\n", __FUNCTION__, dev_name);
        goto ERR;
    	}

	/* Add RAW meaase to SKB */
	skb_put(skb,sizeof(skb_send));
	memmove((void *)skb->data, (void *)(skb->data+sizeof(skb_send)), 2*ETH_ALEN);
	memcpy(skb->data, skb_send, sizeof(skb_send));
	add_marvel_tag(skb);

	return 1;
ERR:
	return -1;

}

void send_p(void) 
{
	
	struct sk_buff *skb;
    	skb = dev_alloc_skb(SKBBUFF);
	//skb->len=TX_BUF_SIZE;
	if(!skb) {
		printk("No memory!!!\n");
		return;
	}
	
	if(packet_make_frame(skb, "eth2") > 0) {

	int j =0;
	while (j<102) {
	printk(KERN_INFO"show the skb->data %d: %x\r\n",j,*(skb->data+j));
	j++;
	}
	
	skb->len = traffcon.frame_size+4;
	printk(KERN_INFO"show the skb->len %d\r\n",skb->len);
	
		dev_queue_xmit(skb);
	}
#if	0
	if(packet_make(skb) > 0) {
		add_marvel_tag(skb);
		dev_queue_xmit(skb);
	}
#endif
	else
		printk("Send packet error...\n");
}

void send_p_tmp(char *data, char *interface) 
{
	int i = 0;	
	struct sk_buff *skb;
	skb = dev_alloc_skb(SKBBUFF);
	//skb->len=TX_BUF_SIZE;
	if(!skb) {
		printk("No memory!!!\n");
		return;
	}

    skb_reserve(skb, 2);
    /* getting & checking net device whether exists  */
    skb->dev = dev_get_by_name(&init_net, interface);

    /* Add RAW meaase to SKB */
    skb_put(skb, 200);
#if 0
	printk("\n\n");
	for(i = 0; i <= 50; i++) {
		printk("%x ", *(data+i));
	}
#endif
    memcpy(skb->data, data, 200);
    add_marvel_tag(skb);

	dev_queue_xmit(skb);
}
static void loopcheck(unsigned long data)
{

	TIMER_PRIV *dp = (TIMER_PRIV *) data;
	struct sk_buff	*skb;	
	int i = 0;
	skb = dev_alloc_skb(SKBBUFF);
printk("\n%s %d %d %d\n", dp->interface, dp->freq, dp->count, INT_NUM);
    if(!skb) {
        printk("No memory!!!\n");
        return;
    }
    skb_reserve(skb, 2);
    skb->dev = dev_get_by_name(&init_net, dp->interface);
    if (!skb->dev) {
		printk ("%s: __dev_get_by_name(%s) fail\n", __FUNCTION__, dev_name);
        return;
    }


    skb_put(skb, dp->pak_len);
    memcpy(skb->data, dp->data, dp->pak_len);
    add_marvel_tag(skb);
    if (dp->count != 0 ) {
        dp->count--;
        dev_queue_xmit(skb);
		lp_timer[dp->index].expires = jiffies + (timer_data[dp->index].freq)*HZ;
        add_timer(&lp_timer[dp->index]);
    } else {
        kfree(dp->data);
        del_timer(&lp_timer[dp->index]);    
    }
}

static void hand_off_timer(char *data, struct pg_control *pg_ctl)
{
	int timer_idx = 0;
	int i = 0;
	/* Get control setting */
	if((pg_ctl->interface < 0) || (pg_ctl->interface > INT_NUM)) {
		printk(KERN_ALERT "Interface index ERROR\n");
		pg_ctl->interface = 0;
	}
	timer_idx = pg_ctl->interface;
	timer_data[timer_idx].index = timer_idx;
	memset(timer_data[timer_idx].interface, 0x0, sizeof(timer_data[timer_idx].interface));
	sprintf(timer_data[timer_idx].interface, "eth%d", pg_ctl->interface);
	timer_data[timer_idx].freq = pg_ctl->freq;
	timer_data[timer_idx].count = pg_ctl->count;
	timer_data[timer_idx].pak_len = pg_ctl->pak_len;
	timer_data[timer_idx].data = kmalloc(pg_ctl->pak_len, GFP_ATOMIC);
	memcpy(timer_data[timer_idx].data, data, pg_ctl->pak_len);

	/* init timer */	
	init_timer(&lp_timer[timer_idx]);
	lp_timer[timer_idx].function = loopcheck;
	lp_timer[timer_idx].data = (unsigned long) &timer_data[timer_idx];
	lp_timer[timer_idx].expires = jiffies + (timer_data[timer_idx].freq)*HZ;
	add_timer(&lp_timer[timer_idx]);
}

void lpgen_info(struct sk_buff *skb)
{	
	int i = 0;
	char interface[8];
	struct pg_control *pg_ctl;
	struct nlmsghdr *nlh = (struct nlmsghdr *)skb->data;
	void *data = NLMSG_DATA(nlh);
	pg_ctl = (struct pg_control *)(data);
	/*Dummy for value testing */
	printk("pg_ctl->dummy = %d\n", pg_ctl->dummy);

	hand_off_timer((char *)(data+sizeof(struct pg_control)), pg_ctl);
}
EXPORT_SYMBOL(lpgen_info);
static int read_proc_lpmd(char * page, char **start, off_t offset, int count, int *eof, void *data)
{
	int len = 0;

	len += sprintf(page+len, "%x\n", lpmd_isEnable);
	*eof = 1;
	return len;

}

static int write_proc_lpmd(struct file *file, const char *buffer, unsigned long count, void *data)
{
	int ret = -EFAULT;
	char tmp[1024];

	memset(tmp, 0x0, sizeof(tmp));

	if (count > 0) {
		copy_from_user(tmp, buffer, count);
		if (!strncmp(tmp, "1", 1)) {
			lpmd_isEnable = 1; 
			send_p();
		} else {
			lpmd_isEnable = 0;
		}
	}
	ret = count;
	return ret;
}



static int __init lpmd_module_init (void)
{
	printk(KERN_ALERT "\n kernel: lpmd init.\n");

	proc_lpmd = create_proc_entry("Enable",0666, proc_mkdir("lpmd", NULL));
	if(!proc_lpmd){
		printk(KERN_ERR "Proc build error...\n");
	}
	proc_lpmd->read_proc = read_proc_lpmd;
	proc_lpmd->write_proc = write_proc_lpmd;
	proc_lpmd->data = NULL;

	lpmd_isEnable = 0;

	return 0;
}

static void __exit lpmd_module_exit (void)
{
	remove_proc_entry("lpmd/Enable", NULL);
	remove_proc_entry("lpmd", NULL);
	printk(KERN_ALERT "\n kernel: lpmd exit.\n");
}

module_init(lpmd_module_init);
module_exit(lpmd_module_exit);

