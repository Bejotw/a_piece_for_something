/*------------------------------------------------------------------
 * lpmd.h
 * Header file for RGMII loopback kernel loadable module.
 *
 * BeJo, Jun. 2012
 *
 * All rights reserved.
 *------------------------------------------------------------------
 */

#ifndef __LPMD_H__
#define __LPMD_H__
#include <linux/netlink.h>

struct pgent_format_send
{	
	int freq;
	int repeat_times;
	int frame_size;
	int data_size;
	unsigned char dmac[6];
	unsigned int eth_type;
	unsigned int ip_ttl;
	int ip_ptotocol;
	unsigned int ip_src;
	unsigned int ip_dst;
	unsigned char data[150];
		struct {
		int icmp_type;
		} icmp;
		struct {
		int udp_source;
		int udp_dest;
		} udp;
};

struct traffic_control
{
	__u16 freq;
	__u16 repeat_times;
	__u16 frame_size;
};

struct eth_header_info
{
	unsigned char dmac[6];
	__u16 eth_type;

};

struct ip_header_info
{
	__u8 ip_ttl;
        __u8 ip_ptotocol;
        __be32 ip_src;
        __be32 ip_dst;
	
};

struct net_header_info
{
	union {
		struct {
			__u8 icmp_type;
		} icmp;
		struct {
			__be16 udp_source;
			__be16 udp_dest;
		} udp;
	} un;
	unsigned char data[150];
};

struct pg_control
{   
    u_int16_t freq;
    u_int16_t count;
    u_int16_t interface;
	u_int16_t pak_len;
    u_int32_t dummy;
};


#endif /* __LPMD_H__ */
